FROM continuumio/miniconda2:4.7.10

LABEL name="PyGRB" \
      maintainer="Tessa Carver <tessa.carver@ligo.org>"

SHELL ["/bin/bash", "-c"]

RUN /opt/conda/bin/conda config --system --prepend channels conda-forge && \
    /opt/conda/bin/conda config --system --append channels igwn && \
    /opt/conda/bin/conda update --yes --quiet conda && \
    /opt/conda/bin/conda clean --all --quiet --yes --force-pkgs-dirs

# get CVMFS path for lalsuite-extra
ENV LAL_DATA_PATH /cvmfs/oasis.opensciencegrid.org/ligo/sw/lalsuite/lalsuite-extra-1.3.0/data/lalsimulation

# install dependencies of pycbc
RUN /opt/conda/bin/conda install --yes --name base \
        "astropy=2.0.3" \
        "beautifulsoup4>=4.6.0" \
        "corner>=2.0.1" \
        "decorator>=3.4.2" \
        "dqsegdb=1.6.0" \
        "emcee=2.2.1" \
        "h5py>=2.5" \
        "ipython" \
        "kombine>=0.8.2" \
        "lscsoft-glue=1.59.3" \
        "mako>=1.0.1" \
        "matplotlib=2.2.3" \
        "mpld3>=0.3" \
        "numpy<1.15.3" \
        "pillow" \
        "pip" \
        "python=2.7" \
        "python-pegasus-wms" \
        "scipy=1.2.1" \
        "setuptools=40.2.0" \
        "unittest2" \
        "weave>=0.16.0" && \
    /opt/conda/bin/conda clean --all --quiet --yes --force-pkgs-dirs

# install dependencies of lalsuite
RUN /opt/conda/bin/conda install --yes --name base \
        "autoconf" \
        "automake<1.16.0a0" \
        "c-compiler" \
        "coreutils" \
        "fftw" \
        "gsl" \
        "hdf5" \
        "libtool" \
        "ldas-tools-framecpp" \
        "libframe" \
        "make" \
        "metaio" \
        "pkg-config" \
        "swig" \
        "zlib" && \
    /opt/conda/bin/conda clean --all --quiet --yes --force-pkgs-dirs

# set build flags for conda (needs to be done after installing c-compiler)
ENV PREFIX /opt/conda
ENV CONDA_BUILD 1

# install lalsuite
RUN source activate base && \
    git clone https://git.ligo.org/andrew-williamson/lalsuite.git && \
    cd lalsuite && \
    git checkout pygrb_o3a && \
    ./00boot && \
    rm -rf _build && mkdir -p _build && cd _build && \
    ../configure \
        --prefix=${PREFIX} \
        --disable-doxygen \
        --disable-gcc-flags \
        --disable-lalstochastic \
        --disable-lalinference \
        --disable-laldetchar \
        --disable-swig-octave \
        --enable-python \
        --enable-swig-python && \
    make -j && \
    make -j install && \
    touch ${CONDA_PREFIX}/lib/python2.7/site-packages/lalsuite-9999.9-py27.egg-info && \
    cd ../../ && \
    rm -rf lalsuite

RUN echo "export LAL_DATA_PATH=${LAL_DATA_PATH}" >> /opt/conda/etc/conda/activate.d/activate-lalsuite-extra.sh

# install pycbc-pylal
RUN source activate base && \
    /opt/conda/bin/python -m pip install git+https://github.com/gwastro/pycbc-pylal.git

# install pycbc
RUN source activate base && \
    git clone https://github.com/a-r-williamson/pycbc.git && \
    cd pycbc && \
    git checkout pygrb_o3a && \
    /opt/conda/bin/python -m pip install . && \
    cd - && \
    rm -rf pycbc

ENV CONDA_BUILD=
